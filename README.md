# Customizing tool for shell #


### What is this repository for? ###

* This repo is basically just for customizing your shell with your name and specific color

### How do I get set up? ###

* pull the repo
* Go to the main directory (customized shell)
* run sudo bash customize.sh

### Contribution guidelines ###

* You want to change the script values?
* Create your ascii art name here: http://patorjk.com/software/taag/#p=display&h=1&v=3&f=Alpha&t=Aminata
* Replace the text value in the custombashrc file
* run sudo bash customize.sh